package juego;

import entorno.Entorno;
import entorno.InterfaceJuego;
import java.util.ArrayList;

public class Juego extends InterfaceJuego {
    private Entorno entorno;
    private Fondo fondo;
    private Princesa princesa;
    private ArrayList<Dinosaurio> dinosaurios;
    private ArrayList<BalaDino> bombas;
    private Piso pisoBase;
    private Piso[] pisos;
    private ArrayList<BalasPrincesa> balas;
    private ArrayList<BalaDino> bombasRemover;
    private boolean juegoTerminado;
   

    public Juego() {
        this.entorno = new Entorno(this, "Titulo de TP - Grupo 5 - Varchetta - Gonzalez - Franco? - V0.01", 800, 700);
        this.fondo = new Fondo("fondo.png", entorno.getWidth() / 2, (entorno.getHeight() / 3) + 50);
        this.pisoBase = new Piso(16, 650, true); // Piso inferior, completamente indestructible
        inicializarDinosaurios();

        this.pisos = new Piso[]{
            pisoBase,
            new Piso(16, 490, false),
            new Piso(16, 330, false),
            new Piso(16, 170, false),
            new Piso(16, 10, false)
        };

        this.princesa = new Princesa(400, pisoBase.getYInicio(), 2.0);
        this.balas = new ArrayList<>();
        this.bombas = new ArrayList<>();
        this.bombasRemover = new ArrayList<>();
        this.juegoTerminado = false;

        this.entorno.iniciar();
    }

    private void inicializarDinosaurios() {
        this.dinosaurios = new ArrayList<>();
        int BajadaPisos = (int) pisoBase.getYInicio() - 210;
        this.dinosaurios.add(new Dinosaurio(0, pisoBase.getYInicio() - 50));
        this.dinosaurios.add(new Dinosaurio(entorno.ancho(), pisoBase.getYInicio() - 50));
        for (int i = 0; i < 6; i++) {
            this.dinosaurios.add(new Dinosaurio(Math.random() * entorno.ancho(), BajadaPisos));
            this.dinosaurios.add(new Dinosaurio(Math.random() * entorno.ancho(), BajadaPisos));
            BajadaPisos = BajadaPisos - 160;
        }
    }

    private boolean colisionaConDino() {
        for (Dinosaurio dino : this.dinosaurios) {
            if (Math.abs(princesa.getX() - dino.getX()) < princesa.getAncho() / 2 + dino.getAncho() / 2 &&
                Math.abs(princesa.getY() - dino.getY()) < princesa.getAlto() / 2 + dino.getAlto() / 2) {
                return true;
            }
        }
        return false;
    }

    private boolean colisionaConBalaDino() {
        for (BalaDino bomba : this.bombas) {
            if (bomba.chocasteCon(princesa)) {
                return true;
            }
        }
        return false;
    }

    public void tick() {
        if (juegoTerminado) {
            entorno.cambiarFont("Arial", 50, java.awt.Color.RED);
            entorno.escribirTexto("Perdiste", 300, 350);
            return;
        }
        
        
        if (princesa.getY() < 0) {
            entorno.cambiarFont("Arial", 50, java.awt.Color.GREEN);
            entorno.escribirTexto("¡Ganaste!", 300, 350);
            return;
        }
 
        fondo.dibujar(entorno);
        princesa.dibujar(entorno);
        for (Piso piso : pisos) {
            piso.dibujar(entorno);
        }
        
        // Dibujar y mover dinosaurios
        for (Dinosaurio dino : this.dinosaurios) {
            dino.dibujarse(this.entorno);
            dino.mover();
            if (princesa.getY() == dino.getY() && this.bombas.size() < 2) {
                if (princesa.getX() > entorno.ancho() / 2) {
                    this.bombas.add(dino.lanzarBomba(princesa, entorno, "derecha"));
                } else {
                    this.bombas.add(dino.lanzarBomba(princesa, entorno, "izquierda"));
                }
            }
        }

        // Manejo del movimiento de la princesa
        if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA)) {
            princesa.moverHaciaIzquierda(true);
        } else {
            princesa.moverHaciaIzquierda(false);
        }

        if (entorno.estaPresionada(entorno.TECLA_DERECHA)) {
            princesa.moverHaciaDerecha(true);
        } else {
            princesa.moverHaciaDerecha(false);
        }

        if (entorno.estaPresionada(entorno.TECLA_ARRIBA)) {
            princesa.saltar();
        }

        princesa.actualizarPosicion(pisos);

        // Disparo de la bala
        if (entorno.sePresiono(entorno.TECLA_ESPACIO) && balas.size() == 0) {
            String direccionBala = princesa.getDireccion(); // Obtener la dirección de la princesa
            BalasPrincesa bala = new BalasPrincesa(princesa.getX(), princesa.getY(), direccionBala, 5);
            balas.add(bala);
        }

        // Dibujar y eliminar balas de la princesa
        for (int i = 0; i < balas.size(); i++) {
            BalasPrincesa bala = balas.get(i);
            bala.mover();
            bala.dibujarse(entorno); // Dibujar la bala antes de verificar si está fuera de la pantalla
            if (bala.fueraDePantalla(entorno)) {
                balas.remove(i);
                i--;
            }
        }

        // Dibujar y mover bombas
        for (int i = 0; i < bombas.size(); i++) {
            bombas.get(i).dibujarse(entorno);
            bombas.get(i).mover();
            if (bombas.get(i).fueraDePantalla(entorno)) {
                bombasRemover.add(bombas.get(i));
            }
        }
        this.bombas.removeAll(bombasRemover);

        // Verificar colisiones
        if (colisionaConDino() || colisionaConBalaDino()) {
            juegoTerminado = true;
        }

        // Detectar colisión entre balas de princesa y balas de dinosaurio
        for (int i = 0; i < balas.size(); i++) {
            BalasPrincesa balaPrincesa = balas.get(i);
            for (int j = 0; j < bombas.size(); j++) {
                BalaDino balaDino = bombas.get(j);
                if (balaPrincesa.chocasteCon(balaDino)) {
                    // Eliminar la bala de la princesa
                    balas.remove(i);
                    // Eliminar la bala del dinosaurio
                    bombas.remove(j);
                    // Salir del bucle interno para evitar ConcurrentModificationException
                    break;
                }
            }
        }
        
        // Detectar colisión entre balas de princesa y dinosaurios
        for (int i = 0; i < balas.size(); i++) {
            BalasPrincesa balaPrincesa = balas.get(i);
            for (int j = 0; j < dinosaurios.size(); j++) {
                Dinosaurio dino = dinosaurios.get(j);
                if (balaPrincesa.chocasteCon(dino)) {
                    // Eliminar la bala de la princesa
                    balas.remove(i);
                    // Eliminar el dinosaurio
                    dinosaurios.remove(j);
                    // Salir del bucle interno para evitar ConcurrentModificationException
                    break;
                }
            }
        }

    }

    public static void main(String[] args) {
        Juego juego = new Juego();
    }
}

