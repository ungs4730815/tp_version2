package juego;

import java.util.Random;

import entorno.Entorno;

public class Piso {
    private Bloque[] bloques;
    private Random random;
    private double yInicio;

    public Piso(int cantidadBloques, int yInicio, boolean esBase) {
        this.random = new Random();
        this.bloques = new Bloque[cantidadBloques];
        this.yInicio = yInicio;

        boolean ultimoDestructible = false;
        int consecutivosDestructibles = 0;

        for (int i = 0; i < cantidadBloques; i++) {
            boolean destructible;

            if (esBase) {
                destructible = false;
            } else {
                if (ultimoDestructible && consecutivosDestructibles < 2) {
                    destructible = true;
                    consecutivosDestructibles++;
                } else {
                    destructible = random.nextBoolean();
                    if (destructible) {
                        consecutivosDestructibles = 1;
                    } else {
                        consecutivosDestructibles = 0;
                    }
                }
            }

            this.bloques[i] = Bloque.crearBloque(50 + i * 50 - 25, yInicio, destructible);
            ultimoDestructible = destructible;
        }
    }

    public void dibujar(Entorno entorno) {
        for (Bloque bloque : bloques) {
            if (bloque != null) {
                bloque.dibujar(entorno);
            }
        }
    }

    public Bloque[] getBloques() {
        return bloques;
    }

    public void eliminarBloque(Bloque bloque) {
        for (int i = 0; i < bloques.length; i++) {
            if (bloques[i] == bloque) {
                bloques[i] = null;
            }
        }
    }

    public double getYInicio() {
        return yInicio;
    }

	public double getX() {
		// TODO Auto-generated method stub
		return 0;
	}

	public Bloque bloqueDebajoDinosaurio(Dinosaurio dinosaurio) {
		// TODO Auto-generated method stub
		return null;
	}
}
