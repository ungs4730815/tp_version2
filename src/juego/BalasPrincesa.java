package juego;

import entorno.Entorno;
import entorno.Herramientas;
import java.awt.Image;

public class BalasPrincesa {
    private double x;
    private double y;
    private double velocidad;
    private String direccion;
    private Image imagenDerecha;
    private Image imagenIzquierda;

    public BalasPrincesa(double x, double y, String direccion, double velocidad) {
        this.x = x;
        this.y = y;
        this.velocidad = velocidad;
        this.direccion = direccion;
        this.imagenDerecha = Herramientas.cargarImagen("granada_der.png");
        this.imagenIzquierda = Herramientas.cargarImagen("granada_izq.png");
    }

    public void dibujarse(Entorno entorno) {
        if ("derecha".equals(direccion)) {
            entorno.dibujarImagen(this.imagenDerecha, this.x, this.y, 0, 0.10);
        } else if ("izquierda".equals(direccion)) {
            entorno.dibujarImagen(this.imagenIzquierda, this.x, this.y, 0, 0.10);
        }
    }

    public void mover() {
        if ("derecha".equals(direccion)) {
            this.x += this.velocidad;
        } else if ("izquierda".equals(direccion)) {
            this.x -= this.velocidad;
        }
    }

    public boolean fueraDePantalla(Entorno entorno) {
        return this.x < 0 || this.x > entorno.ancho();
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public int getAncho() {
		// TODO Auto-generated method stub
		return 0;
	}
    
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    

    public boolean chocasteCon(BalaDino balaDino) {
        // Calcular la distancia entre las balas en el eje X y Y
        double distanciaX = Math.abs(this.x - balaDino.getX());
        double distanciaY = Math.abs(this.y - balaDino.getY());

        // Calcular la suma de los radios de las balas (suponiendo que tienen el mismo tamaño)
        double sumaRadios = this.imagenDerecha.getWidth(null) * 0.10 / 2 + balaDino.getAncho() / 2;

        // Verificar si la distancia entre las balas es menor que la suma de los radios
        if (distanciaX < sumaRadios && distanciaY < sumaRadios) {
            return true; // Hay colisión
        } else {
            return false; // No hay colisión
        }
    }

	public boolean chocasteCon(Dinosaurio dino) {
		// TODO Auto-generated method stub
		return false;
	}
    
//    public boolean chocasteConDino(Dinosaurio dino) {
//        // Calcular la distancia entre la bala de la princesa y el dinosaurio en el eje X y Y
//        double distanciaX = Math.abs(this.x - dino.getX());
//        double distanciaY = Math.abs(this.y - dino.getY());
//
//        // Calcular la suma de los radios de la bala y el dinosaurio (suponiendo que tienen el mismo tamaño)
//        double sumaRadios = this.imagenDerecha.getWidth(null) * 0.10 / 2 + dino.getAncho() / 2;
//
//        // Verificar si la distancia entre la bala y el dinosaurio es menor que la suma de los radios
//        if (distanciaX < sumaRadios && distanciaY < sumaRadios) {
//            return true; // Hay colisión
//        } else {
//            return false; // No hay colisión
//        }
//    }

}
