package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Princesa {
    private double x;
    private double y;
    private double velocidad;
    private double angulo;
    private double velocidadSalto;
    private boolean saltando;
    private String direccion;
    private Image prinderecha;
    private Image prinizquierda;
    private boolean teclaIzquierdaPresionada;
    private boolean teclaDerechaPresionada;
    private double gravedad = 5.5;
    private boolean enPlataforma;

    public Princesa(int x, double y, double velocidad) {
        this.x = x;
        this.y = y;
        this.velocidad = 3.0;
        this.angulo = 0;
        this.velocidadSalto = 40; 
        this.saltando = false;
        this.prinderecha = Herramientas.cargarImagen("princesaderecha.png");
        this.prinizquierda = Herramientas.cargarImagen("princesaizquierda.png");
        this.direccion = "";
        this.teclaIzquierdaPresionada = false;
        this.teclaDerechaPresionada = false;
        this.enPlataforma = false;
    }

    public void moverHaciaIzquierda(boolean presionada) {
        teclaIzquierdaPresionada = presionada;
        if (presionada) {
            x -= velocidad;
            direccion = "izquierda";
        }
        if (x < 25) {
            x = 25;
        }
    }

    public void moverHaciaDerecha(boolean presionada) {
        teclaDerechaPresionada = presionada;
        if (presionada) {
            x += velocidad;
            direccion = "derecha";
        }
        if (x > 775) {
            x = 775;
        }
    }

    public void saltar() {
        if (enPlataforma) {
            saltando = true;
            enPlataforma = false;
        }
    }

    public void actualizarPosicion(Piso[] pisos) {
        if (saltando) {
            y -= velocidadSalto;
            velocidadSalto -= gravedad;
            if (velocidadSalto <= 0) {
                saltando = false;
                velocidadSalto = 40;
            }
        } else {
            y += gravedad;
        }

        enPlataforma = false;

        for (Piso piso : pisos) {
            for (int i = 0; i < piso.getBloques().length; i++) {
                Bloque bloque = piso.getBloques()[i];
                if (bloque != null && colisionaConBloque(bloque)) {
                    if (bloque.getY() > y) {
                        y = bloque.getY() - 50;
                        enPlataforma = true;
                    } else if (saltando && bloque.isDestructible()) {
                        piso.getBloques()[i] = null;
                        saltando = false;
                        velocidadSalto = 40;
                    } else if (!bloque.isDestructible() && bloque.getY() < y) {
                        y = bloque.getY() + 50;
                        saltando = false;
                        velocidadSalto = 40;
                    }
                }
            }
        }

        if (y > 600) {
            y = 600;
        }
    }

    private boolean colisionaConBloque(Bloque bloque) {
        return x + 25 > bloque.getX() - 25 && x - 25 < bloque.getX() + 25 &&
               y + 25 > bloque.getY() - 25 && y - 25 < bloque.getY() + 25;
    }

    public void dibujar(Entorno entorno) {
        if (direccion.equals("izquierda")) {
            entorno.dibujarImagen(prinizquierda, x, y, angulo, 1);
        } else {
            entorno.dibujarImagen(prinderecha, x, y, angulo, 1);
        }
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public String getDireccion() {
        return direccion;
    }
    
    public int getAncho() {
        return Princesa.getWidth(null);
    }

    private static int getWidth(Object object) {
		// TODO Auto-generated method stub
		return 0;
	}
    
	public int getAlto() {
        return Princesa.getHeight(null);
    }

	private static int getHeight(Object object) {
		// TODO Auto-generated method stub
		return 0;
	}
}
