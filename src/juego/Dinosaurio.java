package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Dinosaurio {
    private double x;
    private double y;
    private double velocidad;
    private String direccion;
    private double ultimaDireccionX; // Nueva variable para almacenar la última dirección de movimiento del dinosaurio
    private Image dinoderecha;
    private Image dinoizquierda;
    private double angulo;

    public Dinosaurio(double x, double y) {
        this.x = x;
        this.y = y;
        this.velocidad = 0.6;
        this.angulo = 0;
        this.direccion = "derecha";
        this.dinoderecha = Herramientas.cargarImagen("dinosaurio.png");
        this.dinoizquierda= Herramientas.cargarImagen("dinosaurio1.png");
    }
    
    public void dibujarse(Entorno entorno) {
        if (this.direccion.equals("derecha")) {
            entorno.dibujarImagen(dinoderecha, this.x, this.y, this.angulo, 0.6);
        } else {
            entorno.dibujarImagen(dinoizquierda, this.x, this.y, this.angulo, 0.6);
        }
    }

    public void mover() {
    	this.ultimaDireccionX = this.velocidad;
        if (this.direccion.equals("derecha")) {
            this.x += this.velocidad;
            if (this.x >= 720) {
                this.direccion = "izquierda";
            }
        } else {
            this.x -= this.velocidad;
            if (this.x <= 80) {
                this.direccion = "derecha";
            }
        }
    }
    
    public double getUltimaDireccionX() {
        return ultimaDireccionX;
    }

    public BalaDino lanzarBomba(Princesa princesa, Entorno entorno, String direccion) {
        return new BalaDino(this.x, this.y - 10, direccion, 1.4);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getAncho() {
        return dinoderecha.getWidth(null); // Revisar
    }

    public double getAlto() {
        return dinoderecha.getHeight(null); // Revisar
    }
    
//    public Bloque bloqueDebajo(Piso[] pisos) {
//        for (Piso piso : pisos) {
//            Bloque bloqueDebajo = piso.bloqueDebajoDinosaurio(this);
//            if (bloqueDebajo != null) {
//                return bloqueDebajo;
//            }
//        }
//        return null;
//    }
    
}


